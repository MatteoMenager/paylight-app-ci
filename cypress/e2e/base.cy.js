describe('Base tests', () => {
  beforeEach(() => {
    cy.visit("/");
  });
  describe("The welcome page", () => {
    it("displays the title of the app", () => {
      cy.get("h1").contains("Paylight");
    });
  });
})