# Paylight frontend

This project contains the frontend part of the Paylight application.

This project is made with Vue JS and Vite.

To run an instance of the web app in development mode, run the following command: `npm run dev`.

To build the web app for production, run the following command: `npm run build`.

To run the web app in production mode, run the following command: `npx serve -s ./dist -l 3000`.

Cypress tests can be run using the GUI (`npx cypress open`) or in headless mode (`npx cypress run --browser chrome`).