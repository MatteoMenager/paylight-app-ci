import { defineStore } from "pinia";
import Axios from "axios";

export const useDefaultStore = defineStore({
  id: "default",
  state: () => ({
    allAPIsActive: false,
    checkingAPIs: false,
    isAuthenticated: false,
    JWTtoken: "",
    backendURL: "http://127.0.0.1:8888/",
    backendAPIURL: "http://127.0.0.1:8888/api/",
    cryptoAPIURL: "http://127.0.0.1:12200/",
    validator1URL: "http://127.0.0.1:12100/",
    merchants: [],
    userData: {
      user: {
        name: "",
        surname: "",
        email: "",
        public_key: "",
      },
      open_channel_actions_allowed: false,
      wallet_balance: 0.0,
      funds_expiration_date: "",
      user_personal_balance: null,
    },
    userHistory: [],
  }),
  persist: true,
  getters: {},
  actions: {
    modifyTestValue(newValue) {
      this.testValue = newValue;
    },
    signout() {
      this.isAuthenticated = false;
      this.JWTtoken = "";
    },
    signin(token) {
      this.isAuthenticated = true;
      this.JWTtoken = token;
    },
    checkAPIs() {
      this.checkingAPIs = true;
      let supposeAPIsActive = true;

      const backendTestURL = this.backendURL + "test";
      const validator1TestURL = this.validator1URL + "test";
      const cryptoAPITestURL = this.cryptoAPIURL;

      // Use Promise.all to wait for all requests to finish
      Promise.all([
        Axios.get(backendTestURL),
        Axios.get(validator1TestURL),
        Axios.get(cryptoAPITestURL)
      ]).then(responses => {
        // Check if all responses have status 200
        responses.forEach(response => {
          if (response.status !== 200) {
            supposeAPIsActive = false;
          }
        });
        this.allAPIsActive = supposeAPIsActive;
        this.checkingAPIs = false;
      }).catch(error => {
        supposeAPIsActive = false;
        this.allAPIsActive = supposeAPIsActive;
        this.checkingAPIs = false;
      });

      // dev code for testing
      // this.checkingAPIs = true;
      // setTimeout(() => {
      //   this.allAPIsActive = true;
      //   this.checkingAPIs = false;
      // }, 1000);
    },
    checkAuthentication() {
      const authenticationTestURL = this.backendAPIURL + "test/auth";
      const jwt = this.JWTtoken;
      const config = {
        headers: { Authorization: 'Bearer ' + jwt }
      };
      Axios.get(authenticationTestURL, config).then(response => {
        if (response.status === 200) {
          this.isAuthenticated = true;
        } else {
          this.isAuthenticated = false;
          this.JWTtoken = "";
        }
      }).catch(error => {
        this.isAuthenticated = false;
        this.JWTtoken = "";
      });
      // dev code for testing
      // this.isAuthenticated = true;
    },
    fetchUserData() {
      const config = {
        headers: { Authorization: "Bearer " + this.JWTtoken },
      };
      Axios.get(this.backendAPIURL + "user/data", config).then((response) => {
        if (response.status === 200) {
          this.userData = response.data;
        } else if (response.status === 401) {
          this.signout();
        }
      }).catch((error) => {
        console.log(error);
        if (error.response) {
          if (error.response.status === 401) {
            this.signout();
          }
        } else if (error.request) {
          this.checkAPIs();
        }
      });
    },
    fetchUserHistory() {
      const config = {
        headers: { Authorization: "Bearer " + this.JWTtoken },
      };
      Axios.get(this.backendAPIURL + "user/history", config).then((response) => {
        if (response.status === 200) {
          this.userHistory = response.data;
        } else if (response.status === 401) {
          this.signout();
        }
      }).catch((error) => {
        console.log(error);
        if (error.response) {
          if (error.response.status === 401) {
            this.signout();
          }
        } else if (error.request) {
          this.checkAPIs();
        }
      });
    },
    fetchMerchants() {
      const config = {
        headers: { Authorization: "Bearer " + this.JWTtoken },
      };
      Axios.get(this.backendAPIURL + "merchants", config).then((response) => {
        if (response.status === 200) {
          this.merchants = response.data;
        } else if (response.status === 401) {
          this.signout();
        }
      }).catch((error) => {
        console.log(error);
        if (error.response) {
          if (error.response.status === 401) {
            this.signout();
          }
        } else if (error.request) {
          this.checkAPIs();
        }
      });
    },
  },
});
