import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router/index.js'
import PrimeVue from 'primevue/config';
import ToastService from 'primevue/toastservice';
import ConfirmationService from 'primevue/confirmationservice';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

// custom
import "./assets/reset.css";
import "./assets/look.css";
import 'primeicons/primeicons.css';

//theme
//import "primevue/resources/themes/lara-light-indigo/theme.css";
import "./assets/theme.css"
    
//core
import "primevue/resources/primevue.min.css";

const app = createApp(App)

const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)
app.use(pinia)
app.use(router)
app.use(PrimeVue);
app.use(ToastService);
app.use(ConfirmationService);
app.mount('#app')
