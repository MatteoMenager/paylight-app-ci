import { createRouter, createWebHashHistory } from 'vue-router'
import WelcomeScreenVue from '../components/WelcomeScreen.vue'
import SignInScreenVue from '../components/SignInScreen.vue'
import RegisterScreenVue from '../components/RegisterScreen.vue'
import PayScreenVue from '../components/PayScreen.vue'
import WalletScreenVue from '../components/WalletScreen.vue'
import UserScreenVue from '../components/UserScreen.vue'
import TopUpScreenVue from '../components/TopUpScreen.vue'
import ExtendScreenVue from '../components/ExtendScreen.vue'
import { computed, ref } from 'vue'

import { useDefaultStore } from '../stores/index'

const routes = [
    // À compléter
    {
      path: '/',
      name : 'welcome',
      component : WelcomeScreenVue
    },
    {
      path: '/signin',
      name : 'signin',
      component : SignInScreenVue
    },
    {
      path: '/register',
      name : 'register',
      component : RegisterScreenVue
    },
    {
      path: '/pay',
      name : 'pay',
      component : PayScreenVue
    },
    {
      path: '/wallet',
      name : 'wallet',
      component : WalletScreenVue
    },
    {
      path: '/user',
      name : 'user',
      component : UserScreenVue
    },
    {
      path: '/topup',
      name : 'topup',
      component : TopUpScreenVue
    },
    {
      path: '/extend',
      name : 'extend',
      component : ExtendScreenVue
    },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})


router.beforeEach((to, from) => {

  const store = useDefaultStore();

  // automatically redirect the user to the pay page if he is authenticated and all APIs are active
  if (store.isAuthenticated && store.allAPIsActive && to.name === 'signin') {
    return { name: 'pay' };
  }

  // sign the user out if he tries to access the register page while being authenticated
  if (store.isAuthenticated && to.name === 'register') {
    store.signout();
  }
  
  // handle the case where APIs are not available anymore but the user has not been redirected to the welcome page, if he tries to access a page other than welcome (important to precise otherwise if he goes to welcome we have a loop) we redirect him to welcome
  if (from.name !== 'welcome' && from.name !== 'signin' && from.name !== 'register' && !store.allAPIsActive && to.name !== 'welcome' ) {
    return { name: 'welcome' };
  }

  // handle the case where the user is not authenticated anymore but has not been redirected to the welcome page, if he tries to access a page other than welcome (important to precise otherwise if he goes to welcome we have a loop) we redirect him to login
  if (from.name !== 'welcome' && from.name !== 'signin' && from.name !== 'register' && !store.isAuthenticated && to.name !== 'welcome' &&  to.name !== 'signin') {
    return { name: 'signin' };
  }
  
  if (to.name !== 'welcome' && to.name !== 'signin' && to.name !== 'register' && !store.isAuthenticated) {
    // if no from route, so redirect to welcome
    // there is no from routes in 2 situations:
    // - url called manually without being on the website before
    // - page refreshed while being on a page requiring authentication
    if (!from.name) {
      return { name: 'welcome' };
    }
    return false;
  }
  
  if (to.name !== 'welcome' && !store.allAPIsActive) {
    // if no from route, so redirect to welcome
    // there is no from routes in 2 situations:
    // - url called manually without being on the website before
    // - allAPIsActive changing from true to false while being on a page requiring APIs to be active
    if (!from.name) {
      return { name: 'welcome' };
    }
    return false;
  }

  if (to.name == 'extend' && !store.userData.open_channel_actions_allowed) {
    return false;
  }

  
})

export default router